package se.kumpulainen.jimmy.bookdiary.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import se.kumpulainen.jimmy.bookdiary.R;

/**
 * Created by Jimmy on 2013-09-27.
 */
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        EditText endDate = (EditText) getActivity().findViewById(R.id.editBookEndDate);
        String selectedDate = String.format("%s/%s/%s", day, month + 1, year);

        SimpleDateFormat selectedDateFormatter = null;
        SimpleDateFormat postDateFormatter = null;
        Date dateObj = null;

        try {
            selectedDateFormatter = new SimpleDateFormat("dd/MM/yyyy");
            dateObj = selectedDateFormatter.parse(selectedDate);
            postDateFormatter = new SimpleDateFormat("dd MMM, yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        endDate.setText(postDateFormatter.format(dateObj));
    }
}
