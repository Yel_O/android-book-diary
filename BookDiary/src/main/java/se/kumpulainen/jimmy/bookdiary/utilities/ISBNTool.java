package se.kumpulainen.jimmy.bookdiary.utilities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.EditText;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;

import se.kumpulainen.jimmy.bookdiary.R;

/**
 * Created by Jimmy on 2013-09-13.
 */
public class ISBNTool extends AsyncTask<String, Void, Boolean> {
    private Activity _activity;
    private ProgressDialog _pd;

    private JSONObject _jsonObj = null;

    private HttpClient _httpClient = new DefaultHttpClient();
    private HttpGet _request = new HttpGet();

    private String _googleBooksAPIUri = "https://www.googleapis.com/books/v1/volumes?q=isbn:%s";
    private String _librisAPIUri = "http://api.libris.kb.se/xsearch?query=%s&format=json";

    public ISBNTool() {
    }

    public ISBNTool(Activity activity) {
        _activity = activity;
    }

    protected void onPreExecute() {
        _pd = ProgressDialog.show(_activity, "Searching", "Please wait", false);
    }

    @Override
    protected Boolean doInBackground(String... isbn) {
        String googleJSONUri = String.format(_googleBooksAPIUri, isbn[0]);
        String librisJSONUri = String.format(_librisAPIUri, isbn[0]);

        Log.i("URI", googleJSONUri);

        try {
            // First, try to get information from the google books API.
            _request.setURI(new URI(googleJSONUri));
            String _content = _httpClient.execute(_request, new BasicResponseHandler());

            if (_content != null)
                _jsonObj = new JSONObject(_content);

            if (_jsonObj.has("items")) {
                _jsonObj = _jsonObj.getJSONArray("items")
                        .getJSONObject(0)
                        .getJSONObject("volumeInfo");
            } else {
                // If there's no information in the google books API, try the Libris API
                _request.setURI(new URI(librisJSONUri));
                _content = _httpClient.execute(_request, new BasicResponseHandler());

                if (_content != null)
                    _jsonObj = new JSONObject(_content);

                if (_jsonObj.getJSONObject("xsearch").getInt("records") >= 1)
                    _jsonObj = _jsonObj.getJSONObject("xsearch")
                            .getJSONArray("list")
                            .getJSONObject(0);
            }
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void setupResults(JSONObject jsonObj) {
        EditText editTitle = (EditText) _activity.findViewById(R.id.editBookTitle);
        EditText editPages = (EditText) _activity.findViewById(R.id.editBookPages);

        String title = "";
        String subtitle = "";
        int pages = 0;

        try {
            title = jsonObj.getString("title");
            if (jsonObj.has("subtitle"))
                subtitle = jsonObj.getString("subtitle");
            if (jsonObj.has("pageCount"))
                pages = jsonObj.getInt("pageCount");
            Log.i("Data", Integer.toString(pages));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (subtitle.length() > 0)
            title = String.format("%s: %s", title, subtitle);

        editTitle.setText(title);
        if (pages > 0)
            editPages.setText(Integer.toString(pages));
    }

    protected void onPostExecute(final Boolean success) {
        setupResults(_jsonObj);
        _pd.dismiss();
    }

    public boolean isISBN(String isbn) {
        String isbn_regex = "^(97(8|9))?\\d{9}(\\d|X)$";
        if (isbn.matches(isbn_regex))
           return true;
        return false;
    }
}
