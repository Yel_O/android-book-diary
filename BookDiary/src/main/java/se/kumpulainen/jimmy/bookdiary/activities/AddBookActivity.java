package se.kumpulainen.jimmy.bookdiary.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import se.kumpulainen.jimmy.bookdiary.R;
import se.kumpulainen.jimmy.bookdiary.ZXingHelper.IntentIntegrator;
import se.kumpulainen.jimmy.bookdiary.ZXingHelper.IntentResult;
import se.kumpulainen.jimmy.bookdiary.fragments.DatePickerFragment;
import se.kumpulainen.jimmy.bookdiary.utilities.ISBNTool;

public class AddBookActivity extends FragmentActivity {
    private IntentIntegrator _ii = new IntentIntegrator(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_book);
    }

    public void searchISBN(View v) {
        EditText isbnBox = (EditText) findViewById(R.id.editISBN);
        Toast toast = null;

        String isbn = isbnBox.getText().toString();

        if (new ISBNTool().isISBN(isbn)) {
            ISBNTool isbnTool = new ISBNTool(this);
            isbnTool.execute(isbn);
        } else {
            toast.makeText(this, "The given ISBN didn't match the ISBN standard, please try" +
                    " again.", Toast.LENGTH_SHORT).show();
        }
    }

    private void setISBNBoxContent(String isbn) {
        EditText isbnBox = (EditText) findViewById(R.id.editISBN);
        isbnBox.setText(isbn);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode,
                resultCode,
                intent);

        if (scanResult.getContents() != null) {
            String isbn = scanResult.getContents();

            if (new ISBNTool().isISBN(isbn))
                this.setISBNBoxContent(isbn);
            else
                Toast.makeText(this, "The information returned from the scanner didn't match" +
                        " the ISBN format, please try again.", Toast.LENGTH_SHORT).show();
        }
    }


    public void showDatePickerDialog(View v) {
        DialogFragment dateFragment = new DatePickerFragment();
        dateFragment.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_book, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_scan:
                _ii.initiateScan();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
